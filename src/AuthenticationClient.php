<?php

declare(strict_types=1);

namespace LaravelPassport\ApiClient;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Handler\CurlHandler;
use GuzzleHttp\HandlerStack;

class AuthenticationClient implements TokenNameInterface
{
    private GuzzleClient $client;

    public function __construct(
        private readonly TokenMiddlewareInterface $middleware,
        array $config,
    ) {
        $config = $this->prepareConfig($config);
        $this->client = new GuzzleClient($config);
    }

    public function __call(string $method, array $arguments): mixed
    {
        return call_user_func_array([
            $this->client,
            $method,
        ], $arguments);
    }

    public function getTokenName(): string
    {
        return $this->middleware->getTokenName();
    }

    private function prepareConfig(array $config): array
    {
        $stack = HandlerStack::create(new CurlHandler);
        $stack->push($this->middleware, $this->getTokenName());

        return [
            'handler' => $stack,
            'base_uri' => $config['base_uri'],
            'headers' => [
                'Content-Type' => 'application/json',
                'X-Requested-With' => 'XMLHttpRequest',
            ],
        ];
    }
}
