<?php

declare(strict_types=1);

namespace LaravelPassport\ApiClient;

use GuzzleHttp\Exception\ClientException;
use Illuminate\Cache\Repository;
use InvalidArgumentException;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\Response as HttpResponse;

/**
 * @method array get($uri, array $options = [])
 * @method array put($uri, array $options = [])
 * @method array post($uri, array $options = [])
 */
class ApiClient
{
    protected array $availableMethods = [
        'get',
        'put',
        'post',
    ];

    public function __construct(
        private readonly AuthenticationClient $client,
        private readonly Repository $cache,
        private readonly string $clientId,
        private readonly string $clientSecret,
    ) {
    }

    public function __call(string $method, array $arguments): array
    {
        if (!in_array($method, $this->availableMethods)) {
            throw new InvalidArgumentException(sprintf('Method %s is not available', $method));
        }

        $closure = fn () => call_user_func_array([
            $this->client,
            $method,
        ], $arguments);

        try {
            return $this->toArray($closure());
        } catch (ClientException $e) {
            switch ($e->getResponse()->getStatusCode()) {
                case HttpResponse::HTTP_UNAUTHORIZED:
                    $this->refreshToken();

                    return $this->toArray($closure());
                case HttpResponse::HTTP_UNPROCESSABLE_ENTITY:
                    return $this->toArray($e->getResponse());
            }

            throw $e;
        }
    }

    protected function toArray(ResponseInterface $response): array
    {
        return json_decode($response->getBody()->getContents(), true);
    }

    public function refreshToken(): void
    {
        $response = $this->client->post('oauth/token', [
            'form_params' => [
                'grant_type' => 'client_credentials',
                'client_id' => $this->clientId,
                'client_secret' => $this->clientSecret,
            ],
        ]);

        $response = $this->toArray($response);

        $this->cache->put($this->client->getTokenName(), $response['access_token'], $response['expires_in']);
    }
}
