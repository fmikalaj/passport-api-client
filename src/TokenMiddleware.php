<?php

declare(strict_types=1);

namespace LaravelPassport\ApiClient;

use Closure;
use Illuminate\Cache\Repository;
use Psr\Http\Message\RequestInterface;

abstract class TokenMiddleware implements TokenMiddlewareInterface, TokenNameInterface
{
    public function __construct(private readonly Repository $cache)
    {
    }

    public function __invoke(callable $handler): Closure
    {
        return function (RequestInterface $request, array $options) use ($handler) {
            $token = $this->getToken();

            if ($token) {
                $request = $request->withAddedHeader(
                    'Authorization',
                    sprintf('%s %s', 'Bearer', $token)
                );
            }

            return $handler($request, $options);
        };
    }

    public function getTokenName(): string
    {
        return static::class;
    }

    protected function getToken(): ?string
    {
        return $this->cache->get($this->getTokenName());
    }
}
