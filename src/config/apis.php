<?php

return [
    'service' => [
        'base_uri' => rtrim(env('SERVICE_API_BASE_URI', ''), '/'),
        'client_id' => env('SERVICE_CLIENT_ID', ''),
        'client_secret' => env('SERVICE_CLIENT_SECRET', ''),
    ],
];
