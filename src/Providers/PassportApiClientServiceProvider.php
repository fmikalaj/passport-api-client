<?php

namespace LaravelPassport\ApiClient\Providers;

use Illuminate\Support\ServiceProvider;

class PassportApiClientServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(): void
    {
        $source = realpath(__DIR__.'/../config/apis.php');

        $this->publishes([
            $source => config_path('apis.php')
        ]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
