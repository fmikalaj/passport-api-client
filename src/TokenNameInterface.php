<?php

declare(strict_types=1);

namespace LaravelPassport\ApiClient;

interface TokenNameInterface
{
    public function getTokenName(): string;
}
