<?php

declare(strict_types=1);

namespace LaravelPassport\ApiClient;

use Closure;

interface TokenMiddlewareInterface
{
    public function __invoke(callable $handler): Closure;
}
